class TDatList : public TDataCom {
  protected:
    PTDatLink pFirst;
    PTDatLink pLast;
    PTDatLink pCurrLink;
    PTDatLink pPrevLink;
    PTDatLink pStop;     // End of list
    int CurrPos;         // Number of current link
    int ListLen;         // Quantity links in list
  protected:
    PTDatLink GetLink ( PTDatValue pVal=NULL, PTDatLink pLink=NULL );
    void      DelLink ( PTDatLink pLink );
  public:
    TDatList();
    ~TDatList() { DelList(); }
    // доступ
    PTDatValue GetDatValue ( TLinkPos mode = CURRENT ) const;
    virtual int IsEmpty()  const { return pFirst==pStop; }
    int GetListLength()    const { return ListLen; }       // Quantity links getter
    int SetCurrentPos ( int pos );
    int GetCurrentPos ( void ) const;
    virtual int Reset ( void );             // Set the pointer to the top of the list
    virtual int IsListEnded ( void ) const;
    int GoNext ( void );                    // Shift right of the current link
                // (=1 after use for the last link in list)
    virtual void InsFirst  ( PTDatValue pVal=NULL ); // Before the first
    virtual void InsLast   ( PTDatValue pVal=NULL );
    virtual void InsCurrent( PTDatValue pVal=NULL ); // Before the current

    virtual void DelFirst  ( void );
    virtual void DelCurrent( void );
    virtual void DelList   ( void );
};
