class THeadRing : public TDatList{
  protected:
    PTDatLink pHead;     // It is header. pFirst - The next link for pHead
  public:
    THeadRing ();
   ~THeadRing ();
    virtual void InsFirst( PTDatValue pVal=NULL ); // Insert after header
    virtual void DelFirst( void );                 // Delete the frist link
};
